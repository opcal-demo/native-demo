# native-demo
Spring Native Demo and Build demo.

## image
### registry.gitlab.com/opcal-demo/native-demo:ni
build from graalvm office image

run with oraclelinux:8-slim

### registry.gitlab.com/opcal-demo/native-demo:ni-ubuntu
build from graalvm office image

run with ubuntu:focal

### registry.gitlab.com/opcal-demo/native-demo:graalvm
build from graalvm custom image

run with ubuntu:focal

### registry.gitlab.com/opcal-demo/native-demo:liberica-nik
build from liberica-nik custom image

run with ubuntu:focal

## registry.gitlab.com/opcal-demo/native-demo:pack
build from paketobuildpacks


