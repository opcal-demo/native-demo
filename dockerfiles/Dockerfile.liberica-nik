FROM registry.gitlab.com/opcal-project/containers/liberica-nik:java17-22 as builder

WORKDIR /build

COPY  ${CI_PROJECT_DIR}/ /build/

RUN chmod +x /build/dockerfiles/docker-entrypoint.sh; \
    chmod +x ./mvnw

RUN ./mvnw clean -U -Pnative -DskipTests package

FROM registry.gitlab.com/opcal-project/containers/ubuntu:focal

WORKDIR /app

COPY --from=builder /build/dockerfiles/docker-entrypoint.sh /docker-entrypoint.sh
COPY --from=builder /build/target/native-demo /app/native-demo

RUN set -eux; \
    groupadd -r opcal --gid=1000; \
    useradd -r -g opcal --uid=1000 -m opcal; \
    chown opcal:opcal "/app"; \
    gosu nobody true

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/app/native-demo"]