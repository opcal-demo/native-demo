package com.example.demo.controller;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class InfoController {

	@Value("${info.demo.arg:null}")
	private String infoArg;

	@PostConstruct
	public void init() {
		log.info("#####\n#####\n#####\n#####");
		log.info("infoArg value [{}] ", infoArg);
		log.info("#####\n#####\n#####\n#####");
	}

	@PreDestroy
	public void stop() {
		log.info("#####\n#####\n#####\n#####");
		log.info("system stop");
		log.info("#####\n#####\n#####\n#####");
	}

	@GetMapping("/info")
	public String info(String na) {
		return na + " - " + infoArg + " - " + System.currentTimeMillis();
	}

}
